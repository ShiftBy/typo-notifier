(function ($, drupalSettings, Drupal) {
    var text, entityId, entityType, fieldName, messageForm;

    function keypressListener(e) {
        if (e.keyCode === 10 || (e.keyCode === 13 && e.ctrlKey)) {
            var selection = document.getSelection();
            if (selection.anchorNode === selection.focusNode) {
                text = selection.toString();
                var root = $(selection.anchorNode).closest('.typo-field')[0];
                if (root && root.dataset.entityId) {
                    var popupText = root.dataset.text;
                    var maxLength = +root.dataset.length;
                    maxLength = maxLength !== -1 ? maxLength : drupalSettings.typo.defaultLength;
                    entityId = root.dataset.entityId;
                    entityType = root.dataset.entityType;
                    fieldName = root.dataset.fieldName;
                    $.get(drupalSettings.typo.getText, {
                        entityId: entityId,
                        entityType: entityType,
                        fieldName: fieldName
                    }, function (data) {
                        popupText = data.popup;
                        if (maxLength === -1 || text.length <= maxLength) {
                            dialogElement.empty();
                            messageForm = $('<input type="text" name="message" placeholder="' + Drupal.t('Enter your message (optional)') + '" style="width: 100%">')
                                .appendTo(dialogElement);
                            $('<div>')
                                .text(popupText)
                                .prependTo(dialogElement);
                            dialog.dialog('open');
                        }
                    });
                }
            }
        }
    }

    function send() {
        var message = messageForm.val();
        $.post(drupalSettings.typo.addPath, {
            text: text,
            entityId: entityId,
            entityType: entityType,
            fieldName: fieldName,
            message: message
        });
        dialog.dialog('close');
    }

    var dialogElement = $('<div>').attr('id', 'dialog-form').appendTo($('body'));
    var dialog = $('#dialog-form').dialog({
        autoOpen: false,
        width: 350,
        modal: true,
        buttons: {
            Cancel: function() {
                dialog.dialog('close');
            },
            Send: send
        }
    });
    window.addEventListener('keypress', keypressListener);
})(jQuery, drupalSettings, Drupal);
