<?php
namespace Drupal\typo\EventSubscriber;

use Drupal\typo\Entity\TypoRecord;
use Drupal\typo\Event\TypoAddEvent;
use Drupal\typo\Event\TypoEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class TypoSubscriber implements EventSubscriberInterface
{
    protected $mailManager;
    protected $languageManager;

    protected $config;

    public function __construct()
    {
        $this->languageManager = \Drupal::languageManager();
        $this->mailManager = \Drupal::service('plugin.manager.mail');
        $this->config = \Drupal::config('typo.settings');
    }

    public static function getSubscribedEvents()
    {
        $events[TypoEvents::ADD_TYPO][] = ['saveEntity', 0];
        $events[TypoEvents::ADD_TYPO][] = ['sendEmail', -1];
        return $events;
    }

    public function sendEmail(TypoAddEvent $event)
    {
        $typo = $event->getTypo();
        $from = \Drupal::config('system.site')->get('mail');
        $to = \Drupal::service('typo.entity_loader')
            ->loadEntityField(
                $typo['typo_entity_type'],
                $typo['typo_entity_id'],
                $typo['field_name']
            )
            ->getValue()[0]['email'];
        $to = $to ? $to : $this->config->get('email');
        $language = $this
            ->languageManager
            ->getDefaultLanguage()
            ->getId();
        $this->mailManager
            ->mail('typo', 'typo_report', $to, $language, $typo, $from);
    }

    public function saveEntity(TypoAddEvent $event)
    {
        try {
            TypoRecord::create($event->getTypo())->save();
        } catch (\Exception $e) {
            $event->stopPropagation();
            throw $e;
        }
    }
}