<?php
namespace Drupal\typo;

use Drupal\typo\Exception\EntityNotFoundException;

class EntityLoader
{
    public function loadEntityField($type, $id, $fieldName)
    {
        $storage = \Drupal::entityTypeManager()->getStorage($type);
        if (!$storage) {
            throw new EntityNotFoundException("Invalid entity type");
        }
        $entity = $storage->load($id);
        if (!$entity) {
            throw new EntityNotFoundException("Invalid entity id");
        }
        $field = $entity->get($fieldName);
        if (!$field) {
            throw new EntityNotFoundException("Invalid field name");
        }
        return $field;
    }
}