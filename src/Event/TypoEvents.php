<?php
namespace Drupal\typo\Event;

final class TypoEvents
{
    const ADD_TYPO = 'typo.event.add_typo';
}