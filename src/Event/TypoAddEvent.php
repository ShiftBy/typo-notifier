<?php
namespace Drupal\typo\Event;

use Symfony\Component\EventDispatcher\Event;

class TypoAddEvent extends Event
{
    protected $typo;

    public function __construct($typo)
    {
        $this->typo = $typo;
    }

    public function getTypo()
    {
        return $this->typo;
    }
}