<?php
namespace Drupal\typo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class TypoSettingsForm extends ConfigFormBase
{
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildForm($form, $form_state);
        $config = $this->config('typo.settings');
        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Admin email address'),
            '#default_value' => $config->get('email'),
            '#required' => true,
        ];
        $form['maxLength'] = [
            '#type' => 'number',
            '#title' => $this->t('Max length'),
            '#default_value' => $config->get('max_length'),
            '#required' => true,
        ];
        $form['popup'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Popup text'),
            '#placeholder' => $this->t('Enter popup text'),
            '#default_value' => $config->get('popup'),
            '#required' => true,
        ];
        $form['limit'] = [
            '#type' => 'number',
            '#title' => $this->t('Limit'),
            '#default_value' => $config->get('limit'),
            '#required' => true,
        ];
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $config = $this->config('typo.settings');
        $config->set('email', $form_state->getValue('email'));
        $config->set('max_length', $form_state->getValue('maxLength'));
        $config->set('popup', $form_state->getValue('popup'));
        $config->set('limit', $form_state->getValue('limit'));
        $config->save();
        parent::submitForm($form, $form_state);
    }

    protected function getEditableConfigNames()
    {
        return ['typo.settings'];
    }

    public function getFormId()
    {
        return 'typo_settings_form';
    }
}
