<?php
namespace Drupal\typo\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\user\Entity\User;

class TypoRecordListBuilder extends EntityListBuilder
{
    public function buildHeader()
    {
        $header['id'] = $this->t('Record ID');
        $header['entity_type'] = $this->t('Entity type');
        $header['entity_id'] = $this->t('Entity');
        $header['time'] = $this->t('Time');
        $header['field_name'] = $this->t('Field name');
        $header['text'] = $this->t('Text');
        $header['user'] = $this->t('User');
        $header['message'] = $this->t('Message');

        return $header + parent::buildHeader();
    }

    public function buildRow(EntityInterface $entity)
    {
        $row['id'] = $entity->id();
        $row['entity_type'] = $entity->get('typo_entity_type')->value;
        $row['entity_id'] = $entity->get('typo_entity_id')->value;
        $row['time'] = $entity->get('timestamp')->value;
        $row['field_name'] = $entity->get('field_name')->value;
        try {
            $fieldName = \Drupal::entityTypeManager()
                ->getStorage($row['entity_type'])
                ->load($row['entity_id'])
                ->get($row['field_name'])
                ->getFieldDefinition()
                ->getLabel();
            $row['field_name'] = $fieldName . '(' . $row['field_name'] . ')';
        } finally {
            $row['text'] = $entity->get('text')->value;
            $row['user'] = User::load(+$entity->get('username')->value)->getDisplayName();
            $row['message'] = $entity->get('message')->value;
            return $row + parent::buildRow($entity);
        }
    }

    public function buildOperations(EntityInterface $entity)
    {
        $link = [];
        try {
            $typoEntityType = $entity->get('typo_entity_type')->value;
            $typoEntityId = $entity->get('typo_entity_id')->value;
            $typoEntity = \Drupal::entityTypeManager()
                ->getStorage($typoEntityType)
                ->load($typoEntityId);
            $editPath = $typoEntity->toUrl('edit-form');
            $link = Link::fromTextAndUrl('edit', $editPath)->toRenderable();
        } finally {
            return $link;
        }
    }
}
