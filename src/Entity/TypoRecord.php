<?php
namespace Drupal\typo\Entity;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\Annotation\ContentEntityType;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * @ContentEntityType(
 *     id = "typo_record",
 *     label = @Translation("Typo record"),
 *     base_table = "typo_records",
 *     handlers = {
 *         "list_builder" = "Drupal\typo\Entity\TypoRecordListBuilder"
 *     },
 *     entity_keys = {
 *         "id" = "id",
 *         "typo_entity_type" = "typo_entity_type",
 *         "typo_entity_id" = "typo_entity_id",
 *         "timestamp" = "timestamp",
 *         "field_name" = "field_name",
 *         "text" = "text",
 *         "username" = "username",
 *         "user_ip" = "user_ip",
 *         "message" = "message"
 *     },
 *     links = {
 *         "collection" = "/typo/list"
 *     }
 * )
 */
class TypoRecord extends ContentEntityBase implements ContentEntityInterface
{
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $t = \Drupal::translation();
        $fields['id'] = BaseFieldDefinition::create('integer')
            ->setLabel($t->translate('ID'))
            ->setDescription($t->translate('ID of the typo record'))
            ->setReadOnly(true);
        $fields['typo_entity_type'] = BaseFieldDefinition::create('string')
            ->setLabel($t->translate('Entity type'))
            ->setDescription($t->translate('Type of the typo entity'));
        $fields['typo_entity_id'] = BaseFieldDefinition::create('integer')
            ->setLabel($t->translate('Entity ID'))
            ->setDescription($t->translate('ID of the typo entity'));
        $fields['timestamp'] = BaseFieldDefinition::create('timestamp')
            ->setLabel($t->translate('Creation time'))
            ->setDescription($t->translate('Creation time of the typo record'));
        $fields['field_name'] = BaseFieldDefinition::create('string')
            ->setLabel($t->translate('Entity field name'))
            ->setDescription($t->translate('Field name of the typo entity'));
        $fields['text'] = BaseFieldDefinition::create('string')
            ->setLabel($t->translate('Typo'))
            ->setDescription($t->translate('Typo text'));
        $fields['username'] = BaseFieldDefinition::create('integer')
            ->setLabel($t->translate('User'))
            ->setDescription($t->translate('Author of the typo record'));
        $fields['user_ip'] = BaseFieldDefinition::create('string')
            ->setLabel($t->translate('User IP'))
            ->setDescription($t->translate('IP of the author'));
        $fields['message'] = BaseFieldDefinition::create('string')
            ->setLabel($t->translate('Additional message'))
            ->setDescription($t->translate('Description of the typo record'))
            ->setRequired(false);
        return $fields;
    }
}
