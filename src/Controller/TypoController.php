<?php
namespace Drupal\typo\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\typo\Event\TypoAddEvent;
use Drupal\typo\Event\TypoEvents;
use Drupal\typo\Exception\EntityNotFoundException;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class TypoController extends ControllerBase
{
    protected $eventDispatcher;

    protected $config;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->config = $this->config('typo.settings');
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('event_dispatcher')
        );
    }

    protected function countTypos(AccountProxyInterface $user, $userIp)
    {
        $yesterday = (new \DateTime('now'))
            ->sub(new \DateInterval('P1D'))
            ->getTimestamp();
        $query = \Drupal::entityQuery('typo_record')
            ->condition('timestamp', $yesterday, '>');
        if ($user->isAuthenticated()) {
            $query = $query->condition('username', $user->id(), '=');
        } else {
            $query = $query->condition('user_ip', $userIp, '=');
        }
        return $query->count()->execute();
    }

    protected function createEntity(Request $request)
    {
        $user = \Drupal::currentUser();
        $entityData = [
            'typo_entity_type' => $request->request->get('entityType'),
            'typo_entity_id' => $request->request->get('entityId'),
            'field_name' => $request->request->get('fieldName'),
            'message' => substr($request->request->get('message').' ', 0, 255),
            'text' => $request->request->get('text'),
            'timestamp' => (new \DateTime('now'))->getTimestamp(),
            'username' => ($user ? $user : User::getAnonymousUser())->id(),
            'user_ip' => $request->getClientIp(),
        ];
        return $entityData;
    }

    public function typoAddAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $user = \Drupal::currentUser();
        $userIp = $request->getClientIp();
        $typosToday = $this->countTypos($user, $userIp);
        $limit = $this->config->get('limit');
        if ($typosToday >= $limit) {
            return new AjaxResponse(null, 403);
        }
        $maxLength = $this->config->get('max_length');
        $text = $request->request->get('text');
        if (strlen($text) > $maxLength) {
            return new AjaxResponse(null, 400);
        }
        $typo = $this->createEntity($request);
        $event = new TypoAddEvent($typo);
        $this->eventDispatcher
            ->dispatch(TypoEvents::ADD_TYPO, $event);
        return new AjaxResponse();
    }

    public function typoPopupAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new NotFoundHttpException();
        }
        $typoEntityType = $request->query->get('entityType');
        $typoEntityId = $request->query->get('entityId');
        $fieldName = $request->query->get('fieldName');
        try {
            $field = \Drupal::service('typo.entity_loader')
                ->loadEntityField(
                    $typoEntityType,
                    $typoEntityId,
                    $fieldName
                );
        } catch (EntityNotFoundException $e) {
            return new AjaxResponse(null, 404);
        }
        $text = $field->getValue()[0]['text'];
        return new AjaxResponse([
            'popup' => $text
                ? $text
                : $this->config->get('popup')
        ]);
    }
}
