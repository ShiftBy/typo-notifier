<?php
namespace Drupal\typo\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;

/**
 * @FieldType(
 *   id = "typo_field_type",
 *   label = @Translation("Text (with typo notifier)"),
 *   category = "Text",
 *   default_widget = "typo_widget_type",
 *   default_formatter = "typo_formatter_type"
 * )
 */
class TypoTextFieldType extends TextLongItem implements FieldItemInterface
{
    public static function schema(FieldStorageDefinitionInterface $field_definition)
    {
        $schema = parent::schema($field_definition);
        $schema['columns']['enabled'] = [
            'type' => 'int',
            'size' => 'tiny',
            'not null' => false,
        ];
        $schema['columns']['email'] = [
            'type' => 'varchar',
            'length' => '64',
            'not null' => false,
        ];
        $schema['columns']['length'] = [
            'type' => 'int',
            'not null' => false,
        ];
        $schema['columns']['text'] = [
            'type' => 'text',
            'not null' => false,
        ];
        return $schema;
    }

    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition)
    {
        $t = \Drupal::translation();
        $properties = parent::propertyDefinitions($field_definition);
        $properties['enabled'] = DataDefinition::create('boolean')
            ->setLabel($t->translate('Enable typo notifier'))
            ->setRequired(false);
        $properties['email'] = DataDefinition::create('email')
            ->setLabel($t->translate('Admin email'))
            ->setRequired(false);
        $properties['length'] = DataDefinition::create('integer')
            ->setLabel($t->translate('Max typo text length'))
            ->setRequired(false);
        $properties['text'] = DataDefinition::create('string')
            ->setLabel($t->translate('Popup text'))
            ->setRequired(false);
        return $properties;
    }
}
