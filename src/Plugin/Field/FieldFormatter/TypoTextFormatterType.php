<?php
namespace Drupal\typo\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterInterface;
use Drupal\text\Plugin\Field\FieldFormatter\TextDefaultFormatter;

/**
 * @FieldFormatter(
 *   id = "typo_formatter_type",
 *   label = @Translation("Typo notifier formatter"),
 *   field_types = {
 *     "typo_field_type"
 *   }
 * )
 */
class TypoTextFormatterType extends TextDefaultFormatter implements FormatterInterface
{
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = [];
        $config = \Drupal::config('typo.settings');
        $textItems = parent::viewElements($items, $langcode);
        $needLibrary = false;
        foreach ($items as $key => $value) {
            $elements[$key] = [
                '#type' => 'container',
                '#attributes' => [
                    'class' => [
                        'typo-field',
                    ]
                ],
            ];
            if (+$value->enabled) {
                $needLibrary = true;
                $elements[$key]['#attributes'] += [
                    'data-length' => $value->length,
                    'data-entity-type' => $items->getEntity()->getEntityTypeId(),
                    'data-entity-id' => $items->getEntity()->id(),
                    'data-field-name' => $items->getFieldDefinition()->getName(),
                ];
            }
            $elements[$key]['text'] = $textItems[$key];
        }
        if ($needLibrary) {
            $urlGenerator = \Drupal::urlGenerator();
            $addPath = $urlGenerator->generateFromRoute('entity.typo_record.add');
            $getTextPath = $urlGenerator->generateFromRoute('typo.get_text');
            $elements[0]['#attached']['library'][] = 'typo/typo.listener';
            $elements[0]['#attached']['drupalSettings']['typo']['addPath'] = $addPath;
            $elements[0]['#attached']['drupalSettings']['typo']['getText'] = $getTextPath;
            $elements[0]['#attached']['drupalSettings']['typo']['defaultLength'] = $config->get('max_length');
        }

        return $elements;
    }
}
