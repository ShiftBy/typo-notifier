<?php
namespace Drupal\typo\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * @FieldWidget(
 *   id = "typo_widget_type",
 *   label = @Translation("Typo notifier widget"),
 *   field_types = {
 *     "typo_field_type"
 *   }
 * )
 */
class TypoTextWidgetType extends TextareaWidget implements WidgetInterface
{
    public function formElement(FieldItemListInterface $items,
                                $delta,
                                array $element,
                                array &$form,
                                FormStateInterface $form_state)
    {
        $config = \Drupal::config('typo.settings');
        $email = !empty($items[$delta]->email) ? $items[$delta]->email : $config->get('email');
        $text = !empty($items[$delta]->text) ? $items[$delta]->text : $config->get('popup');
        $length = !empty($items[$delta]->length) && +$items[$delta]->length !== -1
            ? $items[$delta]->length
            : $config->get('max_length');
        $selector = 'input[' . 'name="' . $items->getFieldDefinition()->getName() . '[' . $delta . '][enabled]"]';
        return [
            'enabled' => [
                '#type' => 'checkbox',
                '#title' => 'Typo enabled',
                '#default_value' => !empty($items[$delta]->enabled),
                '#weight' => 201,
            ],
            'container' => [
                '#type' => 'container',
                '#states' => [
                    'invisible' => [
                        $selector => ['checked' => false],
                    ],
                ],
                '#weight' => 201,
                'email' => [
                    '#type' => 'email',
                    '#title' => 'Email',
                    '#default_value' => $email,
                ],
                'length' => [
                    '#type' => 'number',
                    '#title' => 'Max length',
                    '#default_value' => $length,
                    '#min' => '0',
                ],
                'text' => [
                    '#type' => 'textarea',
                    '#title' => 'Popup text',
                    '#default_value' => $text,
                ],
            ],
            'message' => parent::formElement($items, $delta, $element, $form, $form_state) + ['#weight' => 200],
        ];
    }

    public function massageFormValues(array $values,
                                      array $form,
                                      FormStateInterface $form_state)
    {
        $config = \Drupal::config('typo.settings');
        foreach ($values as &$value) {
            $massaged = [];
            $massaged['enabled'] = $value['enabled'];
            $massaged['email'] = $value['container']['email'];
            $massaged['length'] = $value['container']['length'];
            $massaged['text'] = $value['container']['text'];
            $massaged['format'] = $value['message']['format'];
            $massaged['value'] = $value['message']['value'];
            $massaged['length'] = $config->get('max_length') === +$massaged['length']
                ? -1
                : $massaged['length'];
            $massaged['text'] = $config->get('popup') === $massaged['text']
                ? ''
                : $massaged['text'];
            $value = $massaged;
        }
        return parent::massageFormValues($values, $form, $form_state);
    }
}
