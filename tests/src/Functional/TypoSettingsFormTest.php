<?php
namespace Drupal\Tests\typo\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * @group typo
 */
class TypoSettingsFormTest extends BrowserTestBase
{
    public static $modules = ['typo'];
    protected $profile = 'standard';
    protected $webUser;

    public function testNoAccess()
    {
        $this->drupalGet('/typo/settings');
        $this->assertSession()->statusCodeEquals(403);
    }

    public function testForm()
    {
        $email = 'test@test.com';
        $maxLength = 201;
        $popup = 'testText';
        $limit = 10;

        $adminName = $this->randomMachineName();
        $admin = $this->createUser([], $adminName, true);
        $config = $this->config('typo.settings');

        $this->drupalLogin($admin);
        $this->drupalGet('/typo/settings');

        $session = $this->assertSession();
        $session->statusCodeEquals(200);
        $session->fieldValueEquals('email', $config->get('email'));
        $session->fieldValueEquals('maxLength', $config->get('max_length'));
        $session->fieldValueEquals('popup', $config->get('popup'));
        $session->fieldValueEquals('limit', $config->get('limit'));

        $this->drupalPostForm(null, [
            'email' => $email,
            'maxLength' => $maxLength,
            'popup' => $popup,
            'limit' => $limit,
        ], t('Save configuration'));

        $this->drupalGet('/typo/settings');
        $session->statusCodeEquals(200);

        $session->fieldValueEquals('email', $email);
        $session->fieldValueEquals('maxLength', $maxLength);
        $session->fieldValueEquals('popup', $popup);
        $session->fieldValueEquals('limit', $limit);

        $this->drupalLogout();
    }
}